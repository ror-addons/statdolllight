
Statdoll = {}


local TIME_DELAY = 1
local timeLeft = TIME_DELAY
local SD_COUNT = 16 --46

local STAT_COUNT = 8
local PlayerLevel = GameData.Player.battleLevel
local StatCap = PlayerLevel * 25 + 50
local delayedstart = 0

Base = 0
Statdoll.window = "StatdollWnd"

if ShowBg == nil then ShowBg = 1 end

Statdoll.StatLabels = {}
Statdoll.StatValues = {}
Statdoll.ExtraStatValues = {}
Statdoll.StatII = {}
Statdoll.StatIcons = {}
Statdoll.ExtraStatIcons = {}
Statdoll.StatFriendly = {}
Statdoll.StatIconInfo = {}
Statdoll.CurrentValues = {}
Statdoll.ExtraCurrentValues = {}
Statdoll.BaseValues = {}
Statdoll.StatReference = {}


function Statdoll.CreateWindow()
TextLogAddEntry("Chat", 0, L"<icon=57> Statdoll Light 1.5 Loaded")
	CreateWindow(Statdoll.window, true)

	LayoutEditor.RegisterWindow( "StatdollWnd", L"Statdoll Light", L"Statistics Paperdoll", false, false, true, nil )
	
	Statdoll.Initialize()
	Statdoll.SetReference()
end


function Statdoll.Initialize()
-- CharacterWindow.StatIconInfo[GameData.Stats.STRENGTH].iconNum
--GetString( StringTables.Default.LABEL_STRENGTH )
-- Init the labels	
	Statdoll.StatLabels[0] = "Strength"
	Statdoll.StatLabels[1] = "Ballistic"
	Statdoll.StatLabels[2] = "Intelligence"
	Statdoll.StatLabels[3] = "Willpower"
	Statdoll.StatLabels[4] = "Weaponskill"
	Statdoll.StatLabels[5] = "Initiative"
	Statdoll.StatLabels[6] = "Toughness"
	Statdoll.StatLabels[7] = "Wounds"
	Statdoll.StatLabels[8] = "Armor"
	Statdoll.StatLabels[9] = "Spirital"
	Statdoll.StatLabels[10] = "Elemental"
	Statdoll.StatLabels[11] = "Corporeal"
	Statdoll.StatLabels[12] = "Block"
	Statdoll.StatLabels[13] = "Parry"
	Statdoll.StatLabels[14] = "Dodge"
	Statdoll.StatLabels[15] = "Disrupt"


	
-- Init the icons	
	Statdoll.StatIcons[0] = "Stricon"
	Statdoll.StatIcons[1] = "Balicon"
	Statdoll.StatIcons[2] = "Inticon"
	Statdoll.StatIcons[3] = "Wilicon"
	Statdoll.StatIcons[4] = "Weaicon"
	Statdoll.StatIcons[5] = "Iniicon"
	Statdoll.StatIcons[6] = "Touicon"
	Statdoll.StatIcons[7] = "Wouicon"
	Statdoll.StatIcons[8] = "Armicon"
	Statdoll.StatIcons[9] = "Spiicon"
	Statdoll.StatIcons[10] = "Eleicon"
	Statdoll.StatIcons[11] = "Coricon"
	Statdoll.StatIcons[12] = "Blockicon"
	Statdoll.StatIcons[13] = "Parryicon"
	Statdoll.StatIcons[14] = "Dodgeicon"
	Statdoll.StatIcons[15] = "Disrupticon"
	-- Caster stats --



	
	Statdoll.ExtraStatIcons[0] = "StrPowericon"
	Statdoll.ExtraStatIcons[1] = "BalPowericon"
	Statdoll.ExtraStatIcons[2] = "IntPowericon"
	Statdoll.ExtraStatIcons[3] = "WilPowericon"
	Statdoll.ExtraStatIcons[4] = "Apenicon"
	Statdoll.ExtraStatIcons[5] = "ToBeCriticon"
	Statdoll.ExtraStatIcons[6] = "DmgReduxicon"
	Statdoll.ExtraStatIcons[7] = "Healthicon"
	Statdoll.ExtraStatIcons[8] = "ArmPercicon"
	Statdoll.ExtraStatIcons[9] = "SpiPercicon"
	Statdoll.ExtraStatIcons[10] = "ElePercicon"
	Statdoll.ExtraStatIcons[11] = "CorPercicon"	
	Statdoll.ExtraStatIcons[12] = "ExtraBlockicon"	
	Statdoll.ExtraStatIcons[13] = "ExtraParryicon"
	Statdoll.ExtraStatIcons[14] = "ExtraDodgeicon"	
	Statdoll.ExtraStatIcons[15] = "ExtraDisrupticon"		

	
-- Init the values
	Statdoll.StatValues[0] = "StatdollStrengthValue"
	Statdoll.StatValues[1] = "StatdollBallisticValue"
	Statdoll.StatValues[2] = "StatdollIntelligenceValue"
	Statdoll.StatValues[3] = "StatdollWillpowerValue"
	Statdoll.StatValues[4] = "StatdollWeaponskillValue"
	Statdoll.StatValues[5] = "StatdollInitiativeValue"
	Statdoll.StatValues[6] = "StatdollToughnessValue"
	Statdoll.StatValues[7] = "StatdollWoundsValue"
	Statdoll.StatValues[8] = "StatdollArmorValue"
	Statdoll.StatValues[9] = "StatdollSpiritValue"
	Statdoll.StatValues[10] = "StatdollElemValue"
	Statdoll.StatValues[11] = "StatdollCorpValue"
	Statdoll.StatValues[12] = "StatdollBlockValue"
	Statdoll.StatValues[13] = "StatdollParryValue"
	Statdoll.StatValues[14] = "StatdollDodgeValue"
	Statdoll.StatValues[15] = "StatdollDisruptValue"

	
	Statdoll.ExtraStatValues[0] = "StatdollStrPowerValue"
	Statdoll.ExtraStatValues[1] = "StatdollBalPowerValue"
	Statdoll.ExtraStatValues[2] = "StatdollIntPowerValue"
	Statdoll.ExtraStatValues[3] = "StatdollWilPowerValue"
	Statdoll.ExtraStatValues[4] = "StatdollApenValue"
	Statdoll.ExtraStatValues[5] = "StatdollToBeCritValue"
	Statdoll.ExtraStatValues[6] = "StatdollDmgReduxValue"
	Statdoll.ExtraStatValues[7] = "StatdollHealthValue"
	Statdoll.ExtraStatValues[8] = "StatdollArmPercValue"
	Statdoll.ExtraStatValues[9] = "StatdollSpiPercValue"
	Statdoll.ExtraStatValues[10] = "StatdollElePercValue"	
	Statdoll.ExtraStatValues[11] = "StatdollCorPercValue"
	Statdoll.ExtraStatValues[12] = "StatdollExtraBlockValue"		
	Statdoll.ExtraStatValues[13] = "StatdollExtraParryValue"	
	Statdoll.ExtraStatValues[14] = "StatdollExtraDodgeValue"		
	Statdoll.ExtraStatValues[15] = "StatdollExtraDisruptValue"		
	-- Caster stats --

		
Statdoll.SetReference()
Statdoll.GetValues()
Statdoll.SetColors()
Statdoll.FormatDefenses()
Statdoll.WriteLabels()
end


function Statdoll.SetReference()
local offhand = (CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps)*0.375
if (CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].dps > 0) and (CharacterWindow.equipmentData[GameData.EquipSlots.RIGHT_HAND].dps > 0) then DualParry = 10;DualWeild = 1.5 else DualParry = 0;DualWeild = 1 end
-- Set reference values
	Statdoll.StatReference[0] = GetBonus(GameData.BonusTypes.EBONUS_STRENGTH, GameData.Player.Stats[GameData.Stats.STRENGTH].baseValue)
	Statdoll.StatReference[1] = GetBonus(GameData.BonusTypes.EBONUS_BALLISTICSKILL, GameData.Player.Stats[GameData.Stats.BALLISTICSKILL].baseValue)
	Statdoll.StatReference[2] = GetBonus(GameData.BonusTypes.EBONUS_INTELLIGENCE, GameData.Player.Stats[GameData.Stats.INTELLIGENCE].baseValue)
	Statdoll.StatReference[3] = GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)
	Statdoll.StatReference[4] = GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)
	Statdoll.StatReference[5] = GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)
	Statdoll.StatReference[6] = GetBonus(GameData.BonusTypes.EBONUS_TOUGHNESS, GameData.Player.Stats[GameData.Stats.TOUGHNESS].baseValue)	
	Statdoll.StatReference[7] = GetBonus(GameData.BonusTypes.EBONUS_WOUNDS, GameData.Player.Stats[GameData.Stats.WOUNDS].baseValue)
	Statdoll.StatReference[8] = GameData.Player.armorValue
	Statdoll.StatReference[9] = GetBonus(GameData.BonusTypes.EBONUS_SPIRIT_RESIST, GameData.Player.Stats[GameData.Stats.SPIRITRESIST].baseValue)
	Statdoll.StatReference[10] = GetBonus(GameData.BonusTypes.EBONUS_ELEMENTAL_RESIST, GameData.Player.Stats[GameData.Stats.ELEMENTALRESIST].baseValue)
	Statdoll.StatReference[11] = GetBonus(GameData.BonusTypes.EBONUS_CORPOREAL_RESIST, GameData.Player.Stats[GameData.Stats.CORPOREALRESIST].baseValue)
	Statdoll.StatReference[12] = GetBonus(GameData.BonusTypes.EBONUS_BLOCK, GameData.Player.Stats[GameData.Stats.BLOCKSKILL].baseValue)
	Statdoll.StatReference[13] = (GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_PARRY, (GameData.Player.Stats[GameData.Stats.PARRYSKILL].baseValue / 100)))
	Statdoll.StatReference[14] = (GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_EVADE, (GameData.Player.Stats[GameData.Stats.EVADESKILL].baseValue / 100)))
	Statdoll.StatReference[15] = ((GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_DISRUPT, (GameData.Player.Stats[GameData.Stats.DISRUPTSKILL].baseValue / 100))))

	end
--(CharacterWindow.equipmentData[GameData.EquipSlots.Left_HAND].dps*0.375)
	
function Statdoll.GetValues()

-- Set current values
	Statdoll.CurrentValues[0] = GetBonus(GameData.BonusTypes.EBONUS_STRENGTH, GameData.Player.Stats[GameData.Stats.STRENGTH].baseValue)
	Statdoll.CurrentValues[1] = GetBonus(GameData.BonusTypes.EBONUS_BALLISTICSKILL, GameData.Player.Stats[GameData.Stats.BALLISTICSKILL].baseValue)
	Statdoll.CurrentValues[2] = GetBonus(GameData.BonusTypes.EBONUS_INTELLIGENCE, GameData.Player.Stats[GameData.Stats.INTELLIGENCE].baseValue)
	Statdoll.CurrentValues[3] = GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)
	Statdoll.CurrentValues[4] = GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)
	Statdoll.CurrentValues[5] = GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)
	Statdoll.CurrentValues[6] = GetBonus(GameData.BonusTypes.EBONUS_TOUGHNESS, GameData.Player.Stats[GameData.Stats.TOUGHNESS].baseValue)
	Statdoll.CurrentValues[7] = GetBonus(GameData.BonusTypes.EBONUS_WOUNDS, GameData.Player.Stats[GameData.Stats.WOUNDS].baseValue)
	Statdoll.CurrentValues[8] = GameData.Player.armorValue
	Statdoll.CurrentValues[9] = GetBonus(GameData.BonusTypes.EBONUS_SPIRIT_RESIST, GameData.Player.Stats[GameData.Stats.SPIRITRESIST].baseValue)
	Statdoll.CurrentValues[10] = GetBonus(GameData.BonusTypes.EBONUS_ELEMENTAL_RESIST, GameData.Player.Stats[GameData.Stats.ELEMENTALRESIST].baseValue)
	Statdoll.CurrentValues[11] = GetBonus(GameData.BonusTypes.EBONUS_CORPOREAL_RESIST, GameData.Player.Stats[GameData.Stats.CORPOREALRESIST].baseValue)
	Statdoll.CurrentValues[12] = GetBonus(GameData.BonusTypes.EBONUS_BLOCK, GameData.Player.Stats[GameData.Stats.BLOCKSKILL].baseValue)
	Statdoll.CurrentValues[13] = (GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_PARRY, (GameData.Player.Stats[GameData.Stats.PARRYSKILL].baseValue / 100)))	
	Statdoll.CurrentValues[14] = (GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_EVADE, (GameData.Player.Stats[GameData.Stats.EVADESKILL].baseValue / 100)))
	Statdoll.CurrentValues[15] = ((GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_DISRUPT, (GameData.Player.Stats[GameData.Stats.DISRUPTSKILL].baseValue / 100))))	

end


function Statdoll.calcextra()
	Statdoll.ExtraCurrentValues[0] = L"+"..math.floor((Statdoll.CurrentValues[0]/5))
	Statdoll.ExtraCurrentValues[1] = L"+"..math.floor((Statdoll.CurrentValues[1]/5))
	Statdoll.ExtraCurrentValues[2] = L"+"..math.floor((Statdoll.CurrentValues[2]/5))
	Statdoll.ExtraCurrentValues[3] = L"+"..math.floor((Statdoll.CurrentValues[3]/5))
	Statdoll.ExtraCurrentValues[4] = math.floor(((Statdoll.CurrentValues[4]/(GameData.Player.level*7.5+50)*.25)*100.0))..L"%"
	Statdoll.ExtraCurrentValues[5] = math.floor((GameData.Player.level*7.5+50)/10/(Statdoll.CurrentValues[5])*100)..L"%"
	Statdoll.ExtraCurrentValues[6] = L"-"..math.floor((Statdoll.CurrentValues[6]/5))
	Statdoll.ExtraCurrentValues[7] = Statdoll.CurrentValues[7]*10
	Statdoll.ExtraCurrentValues[8] = wstring.format(L"%.01f", (Statdoll.CurrentValues[8] * 0.909) / GameData.Player.level)..L"%"
	Statdoll.ExtraCurrentValues[9] = wstring.format(L"%.01f",Statdoll.CurrentValues[9]/ (GameData.Player.level * 0.42))..L"%"
	Statdoll.ExtraCurrentValues[10] = wstring.format(L"%.01f",Statdoll.CurrentValues[10]/ (GameData.Player.level * 0.42))..L"%"
	Statdoll.ExtraCurrentValues[11] = wstring.format(L"%.01f",Statdoll.CurrentValues[11]/ (GameData.Player.level * 0.42))..L"%"
	Statdoll.ExtraCurrentValues[12] = math.floor(((CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].blockRating)/(GameData.Player.level*7.5+50)*20))..L"%"
	Statdoll.ExtraCurrentValues[13] = math.floor(Statdoll.CurrentValues[4] / (GameData.Player.level * 7.5 + 50) * 13.5)..L"%"
	Statdoll.ExtraCurrentValues[14] = math.floor(Statdoll.CurrentValues[5] / (GameData.Player.level * 7.5 + 50) * 13.5)..L"%"
	Statdoll.ExtraCurrentValues[15] = math.floor(Statdoll.CurrentValues[3] / (GameData.Player.level * 7.5 + 50) * 13.5)..L"%"
	-- Caster stats --

end



function Statdoll.SetColors()
	for i=1, SD_COUNT do
		-- unchanged, set white
if (Statdoll.StatValues[i-1] == nil) then return end
if (Statdoll.ExtraStatValues[i-1] == nil) then return end
		
		LabelSetTextColor(Statdoll.StatValues[i-1], 254, 254, 254)
		LabelSetTextColor(Statdoll.ExtraStatValues[i-1], 254, 254, 254)

		if Statdoll.CurrentValues[i-1] > Statdoll.StatReference[i-1] then
			-- buffed, set green
			LabelSetTextColor(Statdoll.StatValues[i-1], 0, 254, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[i-1], 0, 254, 0)
		end
		
		if Statdoll.CurrentValues[i-1] < Statdoll.StatReference[i-1] then
			-- debuffed, set red
			LabelSetTextColor(Statdoll.StatValues[i-1], 254, 0, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[i-1], 254, 0, 0)
		end
	end

	for i=1, STAT_COUNT do
		if Statdoll.CurrentValues[i-1] > StatCap then
			-- softcapped, calculate diminishing returns and set orange
			Statdoll.CurrentValues[i-1] = math.floor( (Statdoll.CurrentValues[i-1] - StatCap) / 2 + StatCap )
			LabelSetTextColor(Statdoll.StatValues[i-1], 254, 128, 0)
		end

		-- while we're looping over the stats, round down to nearest integer for nicer display
		Statdoll.CurrentValues[i-1] = math.floor(Statdoll.CurrentValues[i-1])
	end
Statdoll.calcextra()
end


function Statdoll.FormatDefenses()
	local percCalc = 0

	percCalc = (Statdoll.CurrentValues[8] * 0.909) / GameData.Player.level
		if percCalc > 75 then
			-- at cap, color orange
			LabelSetTextColor(Statdoll.StatValues[8], 254, 128, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[8], 254, 128, 0)
		end

	percCalc = Statdoll.CurrentValues[9]/ (GameData.Player.level * 0.42)
		if percCalc > 40 then
			-- softcapped, calculate diminishing returns and set orange
			percCalc = wstring.format(L"%.01f",Statdoll.CurrentValues[9] / (GameData.Player.level * 1.2604) + 26.6)
			Statdoll.ExtraCurrentValues[9] = wstring.format(L"%.01f",percCalc)..L"%"
			LabelSetTextColor(Statdoll.StatValues[9], 254, 128, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[9], 254, 128, 0)
			end

	percCalc = Statdoll.CurrentValues[10]/ (GameData.Player.level * 0.42)
		if percCalc > 40 then
			-- softcapped, calculate diminishing returns and set orange
			percCalc = wstring.format(L"%.01f",Statdoll.CurrentValues[10] / (GameData.Player.level * 1.2604) + 26.6)
			Statdoll.ExtraCurrentValues[10] = wstring.format(L"%.01f",percCalc)..L"%"
			LabelSetTextColor(Statdoll.StatValues[10], 254, 128, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[10], 254, 128, 0)
		end

	percCalc = Statdoll.CurrentValues[11]/ (GameData.Player.level * 0.42)
		if percCalc > 40 then
			-- softcapped, calculate diminishing returns and set orange
			percCalc = wstring.format(L"%.01f",Statdoll.CurrentValues[11] / (GameData.Player.level * 1.2604) + 26.6)
			Statdoll.ExtraCurrentValues[11] = wstring.format(L"%.01f",percCalc)..L"%"
			LabelSetTextColor(Statdoll.StatValues[11], 254, 128, 0)
			LabelSetTextColor(Statdoll.ExtraStatValues[11], 254, 128, 0)
		end

	percCalc = ((CharacterWindow.equipmentData[GameData.EquipSlots.LEFT_HAND].blockRating)/(GameData.Player.level*7.5+50)*20) + (GetBonus(GameData.BonusTypes.EBONUS_BLOCK, GameData.Player.Stats[GameData.Stats.BLOCKSKILL].baseValue))
	Statdoll.CurrentValues[12] = math.floor(percCalc)..L"%"

	percCalc = (GetBonus(GameData.BonusTypes.EBONUS_WEAPONSKILL, GameData.Player.Stats[GameData.Stats.WEAPONSKILL].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_PARRY, (GameData.Player.Stats[GameData.Stats.PARRYSKILL].baseValue / 100)))
	Statdoll.CurrentValues[13] = math.floor(percCalc+DualParry)..L"%"

	percCalc = (GetBonus(GameData.BonusTypes.EBONUS_INITIATIVE, GameData.Player.Stats[GameData.Stats.INITIATIVE].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_EVADE, (GameData.Player.Stats[GameData.Stats.EVADESKILL].baseValue / 100)))
	Statdoll.CurrentValues[14] = math.floor(percCalc)..L"%"

	percCalc = ((GetBonus(GameData.BonusTypes.EBONUS_WILLPOWER, GameData.Player.Stats[GameData.Stats.WILLPOWER].baseValue)/(GameData.Player.level*7.5+50)*13.5)+(GetBonus(GameData.BonusTypes.EBONUS_DISRUPT, (GameData.Player.Stats[GameData.Stats.DISRUPTSKILL].baseValue / 100))))
	Statdoll.CurrentValues[15] = math.floor(percCalc)..L"%"

	
	
	
end


function Statdoll.WriteLabels()
	for i=1, SD_COUNT do
		sFrn = towstring(Statdoll.StatLabels[i-1])
		sVal = towstring(Statdoll.CurrentValues[i-1])
		xVal = towstring(Statdoll.ExtraCurrentValues[i-1])
		
		
		LabelSetText(Statdoll.StatLabels[i-1], sFrn)
		LabelSetText(Statdoll.StatValues[i-1], sVal)		
		LabelSetText(Statdoll.ExtraStatValues[i-1], xVal)
		end

-- Setting the icons
		LabelSetText(Statdoll.StatIcons[0], L"<icon=100>")
		LabelSetText(Statdoll.StatIcons[1], L"<icon=107>")
		LabelSetText(Statdoll.StatIcons[2], L"<icon=108>")
		LabelSetText(Statdoll.StatIcons[3], L"<icon=102>")
		LabelSetText(Statdoll.StatIcons[4], L"<icon=106>")
		LabelSetText(Statdoll.StatIcons[5], L"<icon=105>")
		LabelSetText(Statdoll.StatIcons[6], L"<icon=103>")
		LabelSetText(Statdoll.StatIcons[7], L"<icon=104>")		
		LabelSetText(Statdoll.StatIcons[8], L"<icon=121>")
		LabelSetText(Statdoll.StatIcons[9], L"<icon=155>")
		LabelSetText(Statdoll.StatIcons[10], L"<icon=162>")
		LabelSetText(Statdoll.StatIcons[11], L"<icon=164>")
		LabelSetText(Statdoll.StatIcons[12], L"<icon=165>")	
		LabelSetText(Statdoll.StatIcons[13], L"<icon=110>")	
		LabelSetText(Statdoll.StatIcons[14], L"<icon=111>")	
		LabelSetText(Statdoll.StatIcons[15], L"<icon=112>")	

			
		LabelSetText(Statdoll.ExtraStatIcons[0], L"<icon=156>")		
		LabelSetText(Statdoll.ExtraStatIcons[1], L"<icon=156>")	
		LabelSetText(Statdoll.ExtraStatIcons[2], L"<icon=156>")
		LabelSetText(Statdoll.ExtraStatIcons[3], L"<icon=156>")
		LabelSetText(Statdoll.ExtraStatIcons[4], L"<icon=166>")
		LabelSetText(Statdoll.ExtraStatIcons[5], L"<icon=105>")
		LabelSetText(Statdoll.ExtraStatIcons[6], L"<icon=103>")
		LabelSetText(Statdoll.ExtraStatIcons[7], L"<icon=161>")
		LabelSetText(Statdoll.ExtraStatIcons[8], L"<icon=121>")	
		LabelSetText(Statdoll.ExtraStatIcons[9], L"<icon=155>")
		LabelSetText(Statdoll.ExtraStatIcons[10], L"<icon=162>")		
		LabelSetText(Statdoll.ExtraStatIcons[11], L"<icon=164>")
		LabelSetText(Statdoll.ExtraStatIcons[12], L"<icon=165>")				
		LabelSetText(Statdoll.ExtraStatIcons[13], L"<icon=106>")
		LabelSetText(Statdoll.ExtraStatIcons[14], L"<icon=105>")
		LabelSetText(Statdoll.ExtraStatIcons[15], L"<icon=102>")

		
		LabelSetText(Statdoll.StatLabels[0], L"Str:")
		LabelSetText(Statdoll.StatLabels[1], L"Bal:")
		LabelSetText(Statdoll.StatLabels[2], L"Int:")
		LabelSetText(Statdoll.StatLabels[3], L"Will:")		
		LabelSetText(Statdoll.StatLabels[4], L"Wpn:")	
		LabelSetText(Statdoll.StatLabels[5], L"Ini:")
		LabelSetText(Statdoll.StatLabels[6], L"Tou:")
		LabelSetText(Statdoll.StatLabels[7], L"Wou:")

		LabelSetText(Statdoll.StatLabels[8], L"Arm:")
		LabelSetText(Statdoll.StatLabels[9], L"Spi:")
		LabelSetText(Statdoll.StatLabels[10], L"Ele:")
		LabelSetText(Statdoll.StatLabels[11], L"Cor:")

		LabelSetText(Statdoll.StatLabels[12], L"Blk:")
		LabelSetText(Statdoll.StatLabels[13], L"Par:")	
		LabelSetText(Statdoll.StatLabels[14], L"Dod:")
		LabelSetText(Statdoll.StatLabels[15], L"Dis:")
	
end

function StatdollToggleBack()
if ShowBg == 0 then ShowBg =1 else ShowBg = 0 end
end

function Statdoll.Update()
	-- Lets poll for current stats to display

	
	
if delayedstart == 0 then Statdoll.SetReference() end



delayedstart=1
	
	Statdoll.GetValues()
	Statdoll.SetColors()
	Statdoll.FormatDefenses()
	Statdoll.WriteLabels()
	
	
if ShowBg == 0 then WindowSetShowing("StatdollWndBackground",false)
else
WindowSetShowing("StatdollWndBackground",true)
end

	
end
