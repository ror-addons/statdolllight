<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Statdoll Light" version="1.5.1" date="1/4/2016">
        <Author name="Sullemunk" />
        <Description text=" Statdoll Light, Provides character attributes in a compact frame, by Sullemunk for ROR" />
        <Dependencies />
        <Files>
            <File name="Statdoll.lua" />
            <File name="Statdoll.xml" />
        </Files>
	<SavedVariables>         
      <SavedVariable name="ShowBg"/>      
    </SavedVariables>              
        <OnInitialize>
            <CallFunction name="Statdoll.CreateWindow" />
        </OnInitialize>
        <OnUpdate>
		<CallFunction name="Statdoll.Update" />
    	  </OnUpdate>
        <OnShutdown />
    </UiMod>
</ModuleFile>